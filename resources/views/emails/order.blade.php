@component('mail::message')
    # Новый заказ

    Был оформлен новый заказ!

    Контактные данные:

    Имя: {{ $data->name }}
    Телефон: {{ $data->phone }}
    Email: {{ $data->email }}
    Сообщение: {{ $data->text }}

@endcomponent