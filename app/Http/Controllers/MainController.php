<?php

namespace App\Http\Controllers;

use App\Mail\SendOrder;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MainController extends Controller
{
    public function index()
    {
        return view('index');
    }

    public function feedback(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:100',
            'phone' => 'required|max:20',
            'email' => 'required|max:100|email',
            'text' => 'required|max:500'
        ]);

        $order = new Order;

        $order->name = $request->name;
        $order->phone = $request->phone;
        $order->email = $request->email;
        $order->text = $request->text;

        $order->save();

        Mail::to('alex.drobot22@gmail.com')
            ->send(new SendOrder($order));

        $response = 'Ваше сообщение успешно отправлено!';

        return response()->json(array('msg'=> $response), 200);
    }

}
